//use assert_matches::assert_matches;
use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn nil() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(name = "ThingyBuilder"))]
    struct Nil {}

    let nil = ThingyBuilder::new().build().unwrap();
    assert!(matches!(nil, Nil {}));
}

#[test]
fn prefix_outer() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(setter(prefix = "set")))]
    struct Small {
        hello: String,
        world: u32,
    }

    let small = Small::builder()
        .set_hello("abc".to_string())
        .set_world(42)
        .build()
        .unwrap();
    assert_eq!(&small.hello, "abc");
    assert_eq!(small.world, 42);
}

#[test]
fn prefix_inner() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Small {
        hello: String,
        #[deftly(builder(setter(prefix = "set")))]
        world: u32,
    }

    let small = Small::builder()
        .hello("abc".to_string())
        .set_world(42)
        .build()
        .unwrap();
    assert_eq!(&small.hello, "abc");
    assert_eq!(small.world, 42);
}

#[test]
fn rename_inner() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Small {
        hello: String,
        #[deftly(builder(setter(name = "set_the_world")))]
        world: u32,
    }

    let small = Small::builder()
        .hello("abc".to_string())
        .set_the_world(42)
        .build()
        .unwrap();
    assert_eq!(&small.hello, "abc");
    assert_eq!(small.world, 42);
}

#[test]
fn prefix_and_rename() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(setter(prefix = "set")))]
    struct Small {
        hello: String,
        #[deftly(builder(setter(name = "set_the_world")))]
        world: u32,
    }

    let small = Small::builder()
        .set_hello("abc".to_string())
        .set_the_world(42)
        .build()
        .unwrap();
    assert_eq!(&small.hello, "abc");
    assert_eq!(small.world, 42);
}

#[test]
fn rename_build() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(build_fn(name = "construct")))]
    struct Small {
        hello: String,
        world: u32,
    }

    let small = Small::builder()
        .hello("abc".to_string())
        .world(42)
        .construct()
        .unwrap();
    assert_eq!(&small.hello, "abc");
    assert_eq!(small.world, 42);
}
