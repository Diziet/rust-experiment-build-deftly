use assert_matches::assert_matches;
use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn defaults() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(build_fn(error = "HowImpolite")))]
    struct Hello {
        greetings: String,
    }
    #[derive(Debug)]
    struct HowImpolite(String);
    impl From<build_deftly::UninitializedFieldError> for HowImpolite {
        fn from(_value: build_deftly::UninitializedFieldError) -> Self {
            HowImpolite("no greetings‽".into())
        }
    }

    let s = Hello::builder().build();
    assert_matches!(s, Err(HowImpolite(s)) if s == "no greetings‽");

    let h = Hello::builder()
        .greetings("well done".into())
        .build()
        .unwrap();
    assert_eq!(&h.greetings, "well done");
}
