use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn sub_builder_example() {
    // adapted from derive-builder-fork-arti docs.
    #[derive(Debug, Clone, Deftly, Eq, PartialEq)]
    #[derive_deftly(Builder)]
    struct Lorem {
        #[deftly(builder(sub_builder))]
        ipsum: Ipsum,
    }
    #[derive(Debug, Clone, Deftly, Eq, PartialEq)]
    #[derive_deftly(Builder)]
    struct Ipsum {
        i: usize,
    }

    let mut lorem = LoremBuilder::default();
    lorem.ipsum().i(42);
    let lorem = lorem.build().unwrap();
    assert_eq!(
        lorem,
        Lorem {
            ipsum: Ipsum { i: 42 }
        }
    );
}

#[test]
fn sub_builder_renamed() {
    // adapted from derive-builder-fork-arti docs.
    #[derive(Debug, PartialEq, Default, Deftly, Clone)]
    #[derive_deftly(Builder)]
    struct Lorem {
        #[deftly(builder(sub_builder(fn_name = "construct"), field(ty = "IpsumConstructor")))]
        ipsum: Ipsum,
    }

    #[derive(Debug, PartialEq, Default, Deftly, Clone)]
    #[derive_deftly(Builder)]
    #[deftly(builder(name = "IpsumConstructor", build_fn(name = "construct")))]
    struct Ipsum {
        i: usize,
    }

    let mut lorem = LoremBuilder::default();
    lorem.ipsum().i(42);
    let lorem = lorem.build().unwrap();
    assert_eq!(
        lorem,
        Lorem {
            ipsum: Ipsum { i: 42 }
        }
    );
}
