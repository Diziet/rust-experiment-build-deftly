use assert_matches::assert_matches;
use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn skip_setters_outer() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(setter(skip)))]
    struct Small {
        hello: String,
        world: u32,
    }

    let small = Small::builder().build().unwrap();
    assert_eq!(&small.hello, "");
    assert_eq!(small.world, 0);
}

#[test]
fn skip_setters_inner() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Small {
        hello: String,
        #[deftly(builder(setter(skip)))]
        world: u32,
    }

    let small = Small::builder().hello("hi".to_string()).build().unwrap();
    assert_eq!(&small.hello, "hi");
    assert_eq!(small.world, 0);

    assert_matches!(
        Small::builder().build(),
        Err(SmallBuilderError::UninitializedField(_))
    );
}

#[test]
fn skip_setters_dflt() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(setter(skip)))]
    struct Small {
        #[deftly(builder(setter(enabled)))]
        hello: String,
        world: u32,
    }

    let small = Small::builder().hello("hi".to_string()).build().unwrap();
    assert_eq!(&small.hello, "hi");
    assert_eq!(small.world, 0);

    assert_matches!(
        Small::builder().build(),
        Err(SmallBuilderError::UninitializedField(_))
    );
}

#[test]
fn custom_setters_outer() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(setter(custom)))]
    struct Small {
        hello: String,
        world: u32,
    }
    impl SmallBuilder {
        fn set_hello(&mut self, s: &str) -> &mut Self {
            self.hello = Some(s.to_string());
            self
        }
        fn make_world_square_of(&mut self, n: u32) -> &mut Self {
            self.world = Some(n * n);
            self
        }
    }

    let small = Small::builder()
        .set_hello("hi")
        .make_world_square_of(11)
        .build()
        .unwrap();
    assert_eq!(&small.hello, "hi");
    assert_eq!(small.world, 121);
}

#[test]
fn custom_setters_inner() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Small {
        #[deftly(builder(setter(custom)))]
        hello: String,
        #[deftly(builder(setter(custom)))]
        world: u32,
    }
    impl SmallBuilder {
        fn set_hello(&mut self, s: &str) -> &mut Self {
            self.hello = Some(s.to_string());
            self
        }
        fn make_world_square_of(&mut self, n: u32) -> &mut Self {
            self.world = Some(n * n);
            self
        }
    }

    let small = Small::builder()
        .set_hello("hi")
        .make_world_square_of(11)
        .build()
        .unwrap();
    assert_eq!(&small.hello, "hi");
    assert_eq!(small.world, 121);
}

#[test]
fn skip_build() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(build_fn(skip)))]
    struct Small {
        hello: String,
        world: u32,
    }
    impl SmallBuilder {
        fn build(&self) -> Small {
            Small {
                hello: "ignored".into(),
                world: 42,
            }
        }
    }

    let small = Small::builder().hello("abc".to_string()).world(99).build();
    assert_eq!(&small.hello, "ignored");
    assert_eq!(small.world, 42);
}
