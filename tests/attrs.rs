use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn derive1() {
    #[derive(Deftly)]
    #[derive_deftly(Builder)]
    #[deftly(builder(derive = "Debug,Eq,PartialEq"))]
    struct MyDebuggable {
        #[allow(dead_code)]
        item: u8,
    }

    assert_eq!(
        format!("{:?}", MyDebuggableBuilder::new()),
        "MyDebuggableBuilder { item: None }"
    );
    let mut b1 = MyDebuggableBuilder::new();
    b1.item(7);
    let mut b2 = MyDebuggableBuilder::new();
    b2.item(7);
    let mut b3 = MyDebuggableBuilder::new();
    b3.item(8);

    assert_eq!(&b1, &b2);
    assert_ne!(&b1, &b3);
}

#[test]
fn derive_via_struct_attr() {
    #[derive(Deftly)]
    #[derive_deftly(Builder)]
    #[deftly(builder(struct_attr = "#[derive(Debug,Eq,PartialEq)]"))]
    struct MyDebuggable {
        #[allow(dead_code)]
        item: u8,
    }

    assert_eq!(
        format!("{:?}", MyDebuggableBuilder::new()),
        "MyDebuggableBuilder { item: None }"
    );
    let mut b1 = MyDebuggableBuilder::new();
    b1.item(7);
    let mut b2 = MyDebuggableBuilder::new();
    b2.item(7);
    let mut b3 = MyDebuggableBuilder::new();
    b3.item(8);

    assert_eq!(&b1, &b2);
    assert_ne!(&b1, &b3);
}

// TODO: I don't have a great way to test field_addr, impl_attr, and setter_attr.
