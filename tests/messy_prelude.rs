macro_rules! messy {
    { $(( $module:ident, $path:expr));* $(;)? } => {paste::paste!{
        $(
        mod [<$module _messy>] {
            #[allow(unused_imports)]
            use {std::io::Error as Result,
                 std::io::Error as Option,
                 std::io::Error as Into,
                 std::io::Error as TryInto,
                 std::io::Error as Default,
                };
            include!($path);
        }
        )*
    }}
}

messy! {
    (simple, "./simple.rs");
    (dflt, "./dflt.rs");
    (cvt, "./cvt.rs");
    (rename, "./rename.rs");
    (validate, "./validate.rs");
}
