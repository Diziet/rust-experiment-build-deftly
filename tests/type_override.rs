use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn basic_type_override() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Thing {
        #[deftly(builder(field(ty = "String", build_fn = "|bld: &Self| bld.s1.clone()")))]
        s1: String,

        #[deftly(builder(
            field_attr = "#[allow(dead_code)]",
            setter(skip),
            field(ty = "()", build_fn = "|_| 7")
        ))]
        seven: u8,

        #[deftly(builder(
            setter(into),
            field(ty = "String", try_build_fn = "Self::parse_that_int")
        ))]
        parsed_int: u32,
    }
    impl ThingBuilder {
        fn parse_that_int(&self) -> Result<u32, ThingBuilderError> {
            self.parsed_int
                .parse::<u32>()
                .map_err(|e| e.to_string().into())
        }
    }

    let t = ThingBuilder::new()
        .s1("hello".into())
        .parsed_int("33")
        .build()
        .unwrap();
    assert_eq!(&t.s1, "hello");
    assert_eq!(t.seven, 7);
    assert_eq!(t.parsed_int, 33);
}
