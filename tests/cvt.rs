use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn into_on_setter() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Small {
        #[deftly(builder(setter(into)))]
        hello: String,
        #[deftly(builder(setter(try_into)))]
        world: u32,
    }

    let s = SmallBuilder::new()
        .hello("123") // a &'static str
        .world(123_u8)
        .expect("infallible")
        .build()
        .unwrap();
    assert_eq!(&s.hello, "123");
    assert_eq!(s.world, 123);

    let s = SmallBuilder::new()
        .hello('🤗') // char
        .world(999_u64)
        .expect("conversion ok")
        .build()
        .unwrap();
    assert_eq!(&s.hello, "🤗");
    assert_eq!(s.world, 999_u32);

    let mut b = SmallBuilder::new();
    let e = b.world(1_u64 << 35);
    assert!(e.is_err());
}

#[test]
fn strip_option_basic() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Small {
        #[deftly(builder(setter(strip_option)))]
        hello: std::option::Option<String>,
        #[deftly(builder(setter(strip_option)))]
        world: std::option::Option<u32>,
    }

    let s = Small::builder()
        .hello("hi there".to_string())
        .world(7)
        .build()
        .unwrap();
    assert_eq!(s.hello.as_deref(), Some("hi there"));
    assert_eq!(s.world, Some(7));
}

#[test]
fn into_and_strip_option() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Small {
        #[deftly(builder(setter(into, strip_option)))]
        hello: std::option::Option<String>,
        #[deftly(builder(setter(try_into, strip_option)))]
        world: std::option::Option<u32>,
    }

    let s = SmallBuilder::new()
        .hello("123") // a &'static str
        .world(123_u8)
        .expect("infallible")
        .build()
        .unwrap();
    assert_eq!(s.hello.as_deref(), Some("123"));
    assert_eq!(s.world, Some(123));
}
