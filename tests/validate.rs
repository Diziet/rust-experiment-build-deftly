use assert_matches::assert_matches;
use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn validation() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    #[deftly(builder(build_fn(validate = "OddBuilder::validate")))]
    struct Odd {
        num: u32,
    }
    impl OddBuilder {
        fn validate(&self) -> std::result::Result<(), String> {
            match self.num {
                Some(n) if n & 1 == 0 => Err(format!("{} isn't odd", n)),
                _ => Ok(()),
            }
        }
    }

    let odd = OddBuilder::new().num(7).build().unwrap();
    assert_eq!(odd.num, 7);

    let even = OddBuilder::new().num(8).build();
    assert_matches!(
        even.err().unwrap(),
        OddBuilderError::ValidationError(s) if s == "8 isn't odd"
    );

    let absent = OddBuilder::new().build();
    assert_matches!(
        absent.err().unwrap(),
        OddBuilderError::UninitializedField(_)
    );
}
