use std::fmt;

#[doc(hidden)]
pub use derive_deftly;

mod derive_builder;

//pub use derive_builder::derive_deftly_template_Builder;

pub mod prelude {
    pub use crate::derive_builder::derive_deftly_template_Builder;
}

#[derive(Clone, Debug)]
pub struct UninitializedFieldError {
    field_name: &'static str,
}

impl UninitializedFieldError {
    pub fn new(field_name: &'static str) -> Self {
        Self { field_name }
    }
    pub fn field_name(&self) -> &'static str {
        self.field_name
    }
}

impl fmt::Display for UninitializedFieldError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Field not initialized: {}", self.field_name)
    }
}
impl std::error::Error for UninitializedFieldError {}

/// Runtime error used when a sub-field's `build` method failed.
#[derive(Debug, Clone)]
pub struct SubfieldBuildError<E>(&'static str, E);

impl<E> SubfieldBuildError<E> {
    /// Wrap an error in a `SubfieldBuildError`, attaching the specified field name.
    pub fn new(field_name: &'static str, sub_builder_error: E) -> Self {
        SubfieldBuildError(field_name, sub_builder_error)
    }

    /// Get the field name of the sub-field that couldn't be built.
    pub fn field_name(&self) -> &'static str {
        self.0
    }

    /// Get the error that was returned for the sub-field
    pub fn sub_builder_error(&self) -> &E {
        &self.1
    }

    /// Decompose the `SubfieldBuildError` into its constituent parts
    pub fn into_parts(self) -> (&'static str, E) {
        (self.0, self.1)
    }
}

impl<E> fmt::Display for SubfieldBuildError<E>
where
    E: fmt::Display,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "in {}: {}", self.0, self.1)
    }
}

#[doc(hidden)]
#[macro_export]
macro_rules! strip_option {
    { $(std::option::)? Option < $t:ty > } => { $t }
}
